import _ from 'lodash';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';

import SearchBar from './search-bar';
import VideoList from './video-list';
import VideoDetails from './video-details';

const API_KEY = "AIzaSyDMyMAp-1bd7HDIaHnhuBnax5zDnAQ5FIA";

class YoutubeApp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('vr360');
    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, videos => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }

    render() {

        const videoSearch = _.debounce(term => { this.videoSearch(term)}, 300);

        return (
            <div className="youtube-app">
                <div className="youtube-app__header">
                    <SearchBar onSearchTermChange={videoSearch}  />
                </div>
                <div className="youtube-app__content">

                    <div className="youtube-app__video-details">
                        <VideoDetails video={this.state.selectedVideo} />
                    </div>

                    <div className="youtube-app__video-list">
                        <VideoList
                            onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                            videos={this.state.videos}
                        />
                    </div>

                </div>

            </div>
        );
    }
}

export default YoutubeApp;