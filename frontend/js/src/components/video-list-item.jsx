import React, { Component } from 'react';

const VideoListItem = ({video, onVideoSelect}) => {

    const imageUrl = video.snippet.thumbnails.default.url;

    return (
        <li onClick={() => onVideoSelect(video)} className="video-list-item">
            <img
                className="video-list-item__thumb"
                src={imageUrl}
            />
            <div className="video-list-item__name">{video.snippet.title}</div>
        </li>
    );
};

export default VideoListItem;