"use strict";

import React from 'react';
import ReactDOM from 'react-dom';
import YoutubeApp from './components/youtube-app';


// Take this component generated HTML and put it on the page (in the DOM)
ReactDOM.render(<YoutubeApp/>, document.querySelector('#youtube-app-container'));